import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Persona } from 'src/app/Modelo/Persona';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  model: NgbDateStruct;
	date: { year: number; month: number; day: number};

  fechaN: Date; 

  persona :Persona=new Persona();
  constructor(private router:Router,private service:ServiceService, private calendar: NgbCalendar) { }

  ngOnInit() {
    this.Editar();
  }

  Editar(){
    let cedula=localStorage.getItem("cedula");
    let objeto ={
      cedula : cedula
    }
    this.service.getPersonaId(objeto)
    .subscribe(data=>{
      this.persona=data.data[0];
      // console.log(this.persona);
      // this.fechaN = new Date(this.persona.fecha_nacimiento);
      // console.log(this.fechaN);
      // let fecha: string = this.persona.fecha_nacimiento.toString();
      // let [ anio, mes, day ] = fecha.split('-');
      // let [dia, resto] = day.split('T');
    })

  }
  Actualizar(persona:Persona){
    console.log(persona)
    this.service.updatePersona(persona)
    .subscribe(data=>{
      this.persona=data;
      alert("Se Actualizo bien");
      this.router.navigate(["listar"]);
    })
  }

}